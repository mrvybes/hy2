#!/bin/bash
clear

export GREEN='\e[32m'
export RED='\033[0;31m'
export BGBLUE='\e[1;44m'
export ORANGE='\033[0;33m'
export BLUE='\033[0;34m'
export PURPLE='\033[0;35m'
export CYAN='\033[0;36m'
export BG='\E[44;1;39m'
export NC='\033[0;37m'
export WHITE='\033[0;37m'

export TRY="[${RED} * ${NC}]"

# Function to check if a command exists
command_exists() {
    command -v "$1" >/dev/null 2>&1
}

# Function to install dependencies if not present
install_dependencies() {
    if ! command_exists figlet; then
        echo "Installing figlet..."
        sudo apt-get update
        sudo apt-get install -y figlet
    fi

    if ! command_exists lolcat; then
        echo "Installing lolcat..."
        sudo apt-get update
        sudo apt-get install -y lolcat
    fi
}

reboot() {
    clear
    echo -e "${GREEN}System will reboot in 3 seconds...${RESET}"
    sleep 3
    echo ""
    echo -e "${RED}rebooting...${RESET}"
    sudo reboot
}

restart_services() {
    sudo systemctl restart hysteria-server
    # Check if the service is active
    GREEN="\033[0;32m"
    RED="\033[0;31m"
    RESET="\033[0m"

    sudo systemctl restart hysteria-server

    # Check if the service is active
    if systemctl is-active hysteria-server >/dev/null; then
        echo -e "VoltsshX Hysteria2 server is ${GREEN}active${RESET}."
    else
        echo -e "VoltsshX Hysteria2 server is ${RED}not active${RESET}."
    fi

    # Check if the service is enabled
    if systemctl is-enabled hysteria-server >/dev/null; then
        echo -e "VoltsshX Hysteria2 server is ${GREEN}enabled${RESET}."
    else
        echo -e "VoltsshX Hysteria2 server is ${RED}not enabled${RESET}."
    fi
    read -n 1 -s -r -p "  Press any key to return ↩︎"
    menu
}

display_config() {
    clear
    # Define colors for better readability
    BLUE='\033[1;34m'
    NC='\033[0m' # No Color

    # Configuration file path
    config_file="/etc/hysteria/client/config.txt"

    # Check if the configuration file exists
    if [ -f "$config_file" ]; then
        # Check if figlet and lolcat are installed
        if command_exists figlet && command_exists lolcat; then
            figlet -k VoltsshX | lolcat && figlet -k Hysteria2 | lolcat
        else
            echo "Please install 'figlet' and 'lolcat' for better text formatting."
        fi

        echo -e "${BLUE}┌────────────────────────────────────────────────────────────────•${NC}"
        echo -e "${BLUE}• Configuration ⤵⤵ •${NC}  ${BLUE}•${NC}  for HTTP Injector ${NC}"
        echo -e "${BLUE}•────────────────────────────────────────────────────────────────•${NC}"

        # Display the content of the configuration file with '• ' at the beginning of each line
        sed 's/^/• /' "$config_file"

        echo -e "${BLUE}•────────────────────────────────────────────────────────────────•${NC}"
    else
        echo "Configuration file not found: $config_file"
    fi

    echo -e ""
    read -n 1 -s -r -p "  Press any key to return ↩︎"
    menu
}


# Function to display VPS information
vps_info() {
    clear
    install_dependencies

    DESCRIPTION=$(lsb_release -sd)
    CODENAME=$(lsb_release -sc)
    KERNEL=$(uname -r)
    COUNTRY=$(curl -s "https://ipinfo.io/city")
    PUBLIC_IP=$(curl -s https://api.ipify.org)
    ORG=$(curl -s "https://ipinfo.io/org")

    # Check if figlet and lolcat are installed
    if command_exists figlet && command_exists lolcat; then
        figlet -k Voltssh-X | lolcat && figlet -k Hysteria | lolcat
    else
        echo "Please install 'figlet' and 'lolcat' for better text formatting."
    fi

    echo -e "$BLUE•──────────────────────────────────────────────•$NC"
    echo -e "$BLUE│$NC       •• VPS INFORMATION ••  $NC"
    echo -e "$BLUE└──────────────────────────────────────────────┘$NC"
    echo -e "$BLUE┌──────────────────────────────────────────────┐$NC"
    echo -e "$BLUE│$NC IP Info : $PUBLIC_IP"
    echo -e "$BLUE│$NC Country : $COUNTRY"
    echo -e "$BLUE│$NC Org     : $ORG"
    echo -e "$BLUE│$NC System  : $DESCRIPTION"
    echo -e "$BLUE│$NC Codename: $CODENAME"
    echo -e "$BLUE│$NC Kernel  : $KERNEL"
    echo -e "$BLUE└──────────────────────────────────────────────┘$NC"

    echo -e "$BLUE•──────────────────────────────────────────────•$NC"
    echo -e ""
    read -n 1 -s -r -p "  Press any key to return ↩︎"
    menu
}

uninstall_hysteria() {
    clear
    RED="\033[0;31m"
    RESET="\033[0m"

    echo -e "${RED}WARNING: This will uninstall VoltsshX Hysteria2 server.${RESET}"
    read -p "Are you sure you want to uninstall? (y/n): " answer

    if [ "$answer" = "y" ]; then

        # Remove installed files (modify this according to your installation)
        sudo rm -rf /etc/hysteria
        sudo rm -rf /etc/volt
        userdel -r hysteria

        # Stop the Hysteria server
        sudo systemctl stop hysteria-server
        sudo rm /etc/systemd/system/hysteria-server.service
        rm -f /etc/systemd/system/multi-user.target.wants/hysteria-server.service
        rm -f /etc/systemd/system/multi-user.target.wants/hysteria-server@*.service
        systemctl daemon-reload
        systemctl daemon-reload
        # Add more removal commands if needed
        # clear banner
        sed -i '/figlet -k Voltssh-X | lolcat/,/echo -e ""/d' ~/.bashrc

        echo -e "${RED}VoltsshX Hysteria2 server has been uninstalled.${RESET}"
    else
        echo "Uninstall aborted. No changes were made."
    fi

    # Press any key to return to the main menu
    read -n 1 -s -r -p "Press any key to exit ↩︎"
    clear
    exit
}

showLog(){
    clear
    echo -e "${BLUE}┌────────────────────────────────────────────────────────────────•${NC}"
        echo -e "${BLUE}• Logs ⤵⤵ for Hysteria ${NC}"
        echo -e "${BLUE}•────────────────────────────────────────────────────────────────•${NC}"
    echo ""
    echo -e "Hint: Ctrl+C to force exit"
    sleep 3
    journalctl --no-pager -e -u hysteria-server.service --output cat -f
    echo ""
    echo -e "${BLUE}•────────────────────────────────────────────────────────────────•${NC}"
    read -n 1 -s -r -p "Press any key to return or Ctrl+C to force exit ↩︎"
    menu
}

menu() {
    clear
    Hy_PORT_HP=$(cat /etc/volt/Hy_PORT_HP)
    Hy_PORT=$(cat /etc/volt/Hy_PORT)

    # Check if hysteria is active
    if [[ $(systemctl is-active hysteria-server) == 'active' ]]; then
        state_hysteria="\e[1m\e[32m[ON]"
    else
        state_hysteria="\e[1m\e[31m[OFF]"
    fi

    # Allow Insecure
    sec="0"
    if [ "$sec" = "0" ]; then
        sec_state="[ON]"
    else
        sec_state="[OFF]"
    fi

    figlet -k Voltssh-X Hysteria2 | lolcat
    echo -e "$BLUE┌─────────────────────────────────────────────────────────────•${NC}"
    echo -e "$BLUE│$NC ${ORANGE}•${NC} Version: ${BLUE}voltx 2.2.2${NC}      ${BLUE}•${NC} IPinfo: $(curl -sS ipv4.icanhazip.com)"
    echo -e "$BLUE│$NC ${ORANGE}•${NC} TG-Channel: ${BLUE}@voltsshhq${NC}    ${BLUE}•${NC} Domain: $(cat /etc/volt/DOMAIN)"
    echo -e "$BLUE│$NC ${ORANGE}•${NC} DevChat: ${BLUE}@voltsshx${NC}        ${BLUE}•${NC} UPTime: $(uptime -p | sed 's/up //')"
    echo -e "$BLUE└─────────────────────────────────────────────────────────────•${NC}"
    echo -e "$BLUE┌─────────────────────────────────────────────────────────────•$NC"
    echo -e "$BLUE│$NC Protocol : [$(cat /etc/volt/Protocol)] ${BLUE}•${NC} Port : ${Hy_PORT} ${BLUE}•${NC} Hopping Port : ${Hy_PORT_HP} $NC"
    echo -e "$BLUE└─────────────────────────────────────────────────────────────•$NC"
    echo -e "$BLUE┌─────────────────────────────────────────────────────────────•$NC"
    echo -e "$BLUE• Default Values ⤵⤵ •$NC  ${BLUE}•${NC} Hysteria2 Running: $state_hysteria ${NC}"
    echo -e "$BLUE•─────────────────────────────────────────────────────────────•$NC"
    echo -e "$BLUE│$NC ${CYAN}[Obfuscation]${NC} ${ORANGE}•${NC} ${WHITE}$(cat /etc/volt/Hy2OBFS)${NC}"
    echo -e "$BLUE│$NC ${CYAN}[Auth-Passwords]${NC} ${ORANGE}•${NC} ${WHITE}$(cat /etc/volt/Hy2PASSWORD)${NC}"
    echo -e "$BLUE│$NC ${CYAN}[Allow Insecure]${NC} ${ORANGE}•${NC} ${GREEN}$sec_state${NC}"
    echo -e "$BLUE└─────────────────────────────────────────────────────────────•$NC"
    echo -e "$BLUE•─────────────────────────────────────────────────────────────┐$NC"
    echo -e " ${BLUE}[01]${NC} ${ORANGE}•${NC} ${WHITE}Display Config${NC}"
    echo -e " ${BLUE}[02]${NC} ${ORANGE}•${NC} ${WHITE}Restart Hysteria Service${NC}"
    echo -e " ${BLUE}[03]${NC} ${ORANGE}•${NC} ${WHITE}System Info${NC}"
    echo -e " ${BLUE}[04]${NC} ${ORANGE}•${NC} ${WHITE}Show logs${NC}"
    echo -e "$BLUE•─────────────────────────────────────────────────────────────┘${NC}"
    echo -e "$BLUE┌─────────────────────────┐${NC}"
    echo -e "$BLUE│$NC ${BLUE}[05]${NC} ${ORANGE}•${NC} ${WHITE}Reboot Server${NC}"
    echo -e "$BLUE│$NC ${RED}[99]${NC} ${RED}•${NC} ${RED}Uninstall${NC}"
    echo -e "$BLUE│$NC ${BLUE}[00]${NC} ${ORANGE}•${NC} ${RED}Exit${NC}$NC"
    echo -e "$BLUE└─────────────────────────┘${NC}"
    echo -e ""
    echo -ne " Select menu : "
    read opt
    case $opt in
    01 | 1)
        display_config
        ;;
    02 | 2)
        restart_services
        ;;
    03 | 3)
        vps_info
        ;;
    04 | 4)
        showLog
        ;;
    05 | 5)
        clear
        reboot
        ;;
    99 | 9)
        uninstall_hysteria
        ;;
    00 | 0)
        clear
        exit
        ;;
    *)
        clear
        menu
        ;;
    esac
}

while [[ $? -eq 0 ]]; do
    menu
done
